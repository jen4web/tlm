$(document).ready(function () {
  $("#answerOptions").hide(); //  hide quiz form buttons until user is ready to play

  // run function upon clicking "Let's Play"
  $("#startGame").click(function () {
    runNewQues(); // run question function to display quiz question
    $("#instructions").hide();
  });

  var correct = 0; // initialize variable to hold number of correctly answered questions
  var incorrect = 0; // initialize variable to hold number of incorrectly answered questions
  var correctAnswer;

  function runNewQues() {
    // Each question displayed will be removed from the questionBank.  Keep displaying questions as long as there are questions left in the bank
    if (questionBank.length > 0) {
      var random = Math.floor(Math.random() * questionBank.length); // randomly choose index for questionBank array
      var quesAnsObj = questionBank[random]; // access object at random index of questionBank array
      var question = quesAnsObj["questionText"]; // access question inside object at randomly chosen index number of questionBank
      var answerText1 = quesAnsObj["answers"][0]; // access first answer option inside object at randomly chosen index number of questionBank
      var answerText2 = quesAnsObj["answers"][1]; // access second answer option inside object at randomly chosen index number of questionBank
      var answerText3 = quesAnsObj["answers"][2]; // access third answer option inside object at randomly chosen index number of questionBank
      var answerText4 = quesAnsObj["answers"][3]; // access fourth answer option inside object at randomly chosen index number of questionBank
      correctAnswer = quesAnsObj["correctAnswer"]; // access correctAnswer inside object at randomly chosen index number of questionBank

      $("#randomQuestionIdx").text(question); // display random question
      $("label[for=answerText1]").text(answerText1); // display first answer option of respective question
      $("label[for=answerText2]").text(answerText2); // display second answer option of respective question
      $("label[for=answerText3]").text(answerText3); // display third answer option of respective question
      $("label[for=answerText4]").text(answerText4); // display fourth answer option of respective question
      questionBank.splice(random, 1); // remove displayed question from possible future questions

      $("#answerOptions").show(); // show answer options for each question
      answerQuestion(); // start timer for answering question
    } else {
      // when all questions in the questionBank have been answered
      alert("Congratulations!  You have completed the quiz!");
    }
  }

  function answerQuestion() {
    var counter = 10; // user has 10 seconds to answer question
    $("#timer").text(""); // clear countdown message for each new question
    var answerTimer = setInterval(function () {
      //  timer

      if (counter > 0) {
        // as long as there is time remaining to answer
        $("#timer").text("You have " + counter + " seconds remaining."); // advise user of time remaining to answer question
        counter--;

        // when user submits answer to question
        $("#submit")
          .unbind()
          .click(function (event) {
            event.preventDefault();
            clearInterval(answerTimer); // stop timer
            $("#timer").text(""); //  clear timer message
            var userAnswer = [];
            $("input:checked").each(function () {
              userAnswer.push(parseInt($(this).val())); // gather user input and push into array
            });
            //var userAnswer = parseInt($(this).val());  // gather value of user input
            var result = true; // initialize result to true
            for (var i = 0; i < correctAnswer.length; i++) {
              if (userAnswer[i] !== correctAnswer[i]) {
                // compare user input to answer of respective question
                result = false;
              }
            }
            if (result === true) {
              correct++; // increase tally of correctly answered questions
              $("#results").html(
                "Correct!<br>You have answered " +
                  correct +
                  " correctly and " +
                  incorrect +
                  " incorrectly.<br>Try the next one!"
              ); //  if the user is correct
            } else {
              incorrect++; // increase tally of incorrectly answered questions
              $("#results").html(
                "Sorry, that's incorrect.  <br>You have answered " +
                  correct +
                  " correctly and " +
                  incorrect +
                  " incorrectly.<br>Try the next one!"
              ); // if the user is incorrect
            }
            //  Display results
            resultsDisplay();
            //  Move on to next question
            nextQ();
          });
      }
      if (counter === 0) {
        // if user fails to answer question in time
        clearInterval(answerTimer); // stop timer
        $("#timer").text("Time's Up!"); // advise user time is up
        //$('input:radio').prop('disabled', true);  // disable user's ability to answer once time is up
        incorrect++; // unanswered question qualifies as incorrectly answered quesion
        $("#results").html(
          "You have answered " +
            correct +
            " correctly and " +
            incorrect +
            " incorrectly.<br>Try the next one!"
        ); // current tally of correct vs. incorrect
        //  Display results
        resultsDisplay();
        //  Move on to next question
        nextQ();
      }
    }, 1000); // one second intervals
  }

  var resultsDisplay = function () {
    $("#results").append('<button id="nextQuestion">Next!</button>'); //  button to move on to next question
    $("#nextQuestion").css({
      "margin-left": "10px",
      padding: "5px",
    });
    $("#results").show(); // display the results
    $("#answerOptions").hide(); // hide answer options to avoid submitting answer again
  };

  var nextQ = function () {
    $("#nextQuestion").click(function () {
      $("#results").hide(); // hide the current results
      $("#nextQuestion").remove(); // remove the button because it will append when the function runs again
      $("input:checked").prop("checked", false); // clear all radio inputs
      //$('input:radio').prop('disabled', false); // enable user to answer next question
      runNewQues(); // run the function again to display a new question
    });
  };
});
